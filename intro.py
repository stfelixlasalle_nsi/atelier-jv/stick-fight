
direction = 2

alien = Actor('alien')

alien.topright = 50, 50

bg_color = (128, 0, 0)
WIDTH = 300
HEIGHT = 300

def draw():
    screen.fill(bg_color)
    alien.draw()

def update():
    global direction
    alien.right += direction
    if alien.right > WIDTH:
        direction = -2
    elif alien.left < 0:
        direction = 2
